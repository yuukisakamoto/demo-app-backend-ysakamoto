package com.example.db.table

import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.CurrentDateTime
import org.jetbrains.exposed.sql.Table
import java.text.SimpleDateFormat

object Todos : Table() {
    val id = long("id").autoIncrement()
    val title = varchar("title",100).nullable()
    val detail = varchar("detail", 1000)
    val date = date("date").nullable()
    private val createdAt = datetime("created_at").defaultExpression(CurrentDateTime())
    private val updatedAt = datetime("updated_at").defaultExpression(CurrentDateTime())
}