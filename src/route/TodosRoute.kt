package com.example.route

import com.example.db.table.Todos
import com.example.repository.*
import com.example.repository.BadRequestException
import com.example.repository.ErrorCode.Companion.DELETE_ERROR_CODE
import com.example.repository.ErrorCode.Companion.GET_ERROR_CODE
import com.example.repository.ErrorCode.Companion.NO_ERROR_CODE
import com.example.repository.ErrorCode.Companion.POST_ERROR_CODE
import com.example.repository.ErrorCode.Companion.PUT_ERROR_CODE
import com.example.repository.ErrorMessage.Companion.DELETE_ERROR_MESSAGE
import com.example.repository.ErrorMessage.Companion.GET_ERROR_MESSAGE
import com.example.repository.ErrorMessage.Companion.NO_ERROR_MESSAGE
import com.example.repository.ErrorMessage.Companion.POST_ERROR_MESSAGE
import com.example.repository.ErrorMessage.Companion.PUT_ERROR_MESSAGE
import com.fasterxml.jackson.core.JsonParseException
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import java.lang.Exception
import kotlin.NullPointerException
import kotlin.text.get

fun Route.todos() {
    val todoRepository = TodoRepository()

    route("/todos") {
        get {
            try {
                call.respond(todoRepository.findAll())
//                InternalServerErrorException(ErrorMessage.NO_ERROR_MESSAGE, ErrorCode.NO_ERROR_CODE)
            } catch (e: Exception) {
                when (e) {
                    is RecordInvalidException -> {
                        //　引数にErrorResponse.ktの値を指定している
                        throw InternalServerErrorException(GET_ERROR_MESSAGE, GET_ERROR_CODE)
                    }
                    else -> throw UnknownException()
                }
            }
        }

        post {
            try {
                val parameter = call.receive<TodoCreateParameter>()
                todoRepository.create(parameter)
                call.respondText("")
            } catch (e: Exception) {
                when (e) {
                    is JsonParseException -> {
                        throw BadRequestException()
                    }
                    is RecordInvalidException -> {
                        throw InternalServerErrorException(POST_ERROR_MESSAGE, POST_ERROR_CODE)
                    }
                    else -> throw UnknownException()
                }
            }
        }

        put("/{id}") {
            try {
                val parameter = call.receive<TodoEditParameter>()
                todoRepository.edit(parameter)
            } catch (e: Exception) {
                when (e) {
                    is NullPointerException, is JsonParseException -> {
                        throw BadRequestException()
                    }
                    is RecordInvalidException -> {
                        throw InternalServerErrorException(PUT_ERROR_MESSAGE, PUT_ERROR_CODE)
                    }
                    else -> throw UnknownException()
                }
            }
        }

        delete("/{id}") {
            try {
                val id = call.parameters["id"]?.toLong() ?: return@delete
                todoRepository.delete(id)
            } catch (e: Exception) {
                when (e) {
                    is NullPointerException -> {
                        throw BadRequestException()
                    }
                    is RecordInvalidException -> {
                        throw InternalServerErrorException(DELETE_ERROR_MESSAGE, DELETE_ERROR_CODE)
                    }
                    else -> throw UnknownException()
                }
            }
        }

    }
}