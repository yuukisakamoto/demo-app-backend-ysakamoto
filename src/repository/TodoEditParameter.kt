package com.example.repository

import org.joda.time.format.DateTimeFormat
import java.time.LocalDate

class TodoEditParameter (
//    PUTリクエストパラメータ用のdata class
    val id: Long,
    val title: String,
    val detail: String?,
    val date: String?,
        )
