package com.example.repository

import org.joda.time.format.DateTimeFormat
import java.time.LocalDate

class TodoCreateParameter (
//    POSTリクエストパラメータ用のdata class
    val title: String,
    val detail: String?,
    val date: String?,
        )
