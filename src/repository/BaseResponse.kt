package com.example.repository

interface BaseResponse {
    val errorCode: Int
    val errorMessage: String
}