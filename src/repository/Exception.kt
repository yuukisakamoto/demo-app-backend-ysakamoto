package com.example.repository

import com.example.repository.ErrorCode.Companion.BAD_REQUEST_ERROR_CODE
import com.example.repository.ErrorCode.Companion.UNKNOWN_ERROR_CODE
import com.example.repository.ErrorMessage.Companion.BAD_REQUEST_ERROR_MESSAGE
import com.example.repository.ErrorMessage.Companion.UNKNOWN_ERROR_MESSAGE
import io.ktor.http.*
import io.ktor.http.HttpStatusCode.Companion.BadRequest
import io.ktor.http.HttpStatusCode.Companion.InternalServerError

//　抽象クラス(abstract class)は抽象メソッド(abstract)を持つクラス
//　抽象クラスは自身だけでは意味を持たず、サブクラスに継承されることで初めて機能する
abstract class SystemException(message: String, private val code: Int? = null, ex: Exception? = null) :
    RuntimeException(message, ex) {
    // HttpStatusCodeはHTTP用に定義されたステータス コードの値(504,505とか)。
    abstract val status: HttpStatusCode
    fun response() = ErrorResponse(
        // ?: の前がnullじゃなかったらそっちを、nullだったら後ろを返す。
        errorCode = code ?: status.value,
        errorMessage = message ?: "error"
    )
}
class RecordInvalidException : Exception()

class UnknownException : SystemException {
    constructor(message: String = UNKNOWN_ERROR_MESSAGE) : super(message, code = UNKNOWN_ERROR_CODE)

    override val status: HttpStatusCode = InternalServerError
}

//SystemExceptionを継承している
class InternalServerErrorException : SystemException {
    constructor(message: String, code: Int) : super(message, code)

    override val status: HttpStatusCode = InternalServerError
}

class BadRequestException : SystemException {
    constructor(message: String = BAD_REQUEST_ERROR_MESSAGE) : super(message, code = BAD_REQUEST_ERROR_CODE)

    override val status: HttpStatusCode = BadRequest
}