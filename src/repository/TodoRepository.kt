package com.example.repository

import com.example.Todo
import com.example.db.table.Todos
import com.fasterxml.jackson.databind.ObjectMapper
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat


class TodoRepository {
//    val todoSb = StringBuilder()
    val formatter = DateTimeFormat.forPattern("yyyy-MM-dd")
    // テーブルへのクエリ実行
    fun findAll(): List<Todo> {
        return transaction {
//            todoSb.append("test")
            Todos.selectAll().sortedBy { it[Todos.date] }.map {
                Todo(
                    title = it[Todos.title].toString(),
                    detail = it[Todos.detail].toString(),
                    date = formatter.print(it[Todos.date]).toString(),
                )
            }
        }
    }

    fun create(parameter: TodoCreateParameter) {
//        テーブルへ新規レコードを追加するメソッド
        transaction {
            Todos.insert {
                it[title] = parameter.title
                it[detail] = parameter.detail.toString()
                it[date] = DateTime.parse(parameter.date, formatter)
            }
        }
    }

    fun edit(parameter: TodoEditParameter) {
//        指定idのレコードを更新するメソッド
        transaction {
            Todos.update(where = { Todos.id eq parameter.id}) {
                it[title] = parameter.title
                it[detail] = parameter.detail.toString()
                it[date] = DateTime.parse(parameter.date, formatter)
            }
        }
    }

    fun delete(id: Long) {
//        指定したidのレコードを削除するメソッド
        transaction {
            Todos.deleteWhere { Todos.id eq id }
        }
    }

}