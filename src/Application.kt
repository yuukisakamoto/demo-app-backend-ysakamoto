package com.example

import com.example.repository.SystemException
import com.example.route.todos
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.http.*
import com.fasterxml.jackson.databind.*
import io.ktor.jackson.*
import io.ktor.features.*
import io.ktor.gson.*
import io.ktor.server.netty.*
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Date
import java.text.SimpleDateFormat
import java.time.LocalDate

fun main(args: Array<String>): Unit = EngineMain.main(args)

private fun connectDB() {
    Database.connect(
        url = "jdbc:mysql://127.0.0.1/todo",
        driver = "com.mysql.cj.jdbc.Driver",
        user = "root",
        password = "P@ssw0rd"
    )
}

// 扱いやすくする為にTodoデータクラス作成
data class Todo(
    val title: String,
    val detail: String?,
    // nullを許容する場合は?をつける
    val date: String?,
)

@Suppress("unused") // Referenced in application.conf
@JvmOverloads
fun Application.module(testing: Boolean = false) {
    // DBへ接続
    connectDB()

    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
        gson {
            // Configure Gson here
        }
    }

    // 例外処理
    install(StatusPages) {
        exception<SystemException> { cause ->
            call.response.status(cause.status)
            call.respond(cause.response())
        }
    }

    routing {
        todos()

        get("/") {
            call.respondText("HELLO WORLD!", contentType = ContentType.Text.Plain)
        }

        get("/json/jackson") {
            call.respond(mapOf("hello" to "world"))
        }

    }
}


