package com.example

import com.example.repository.TodoEditParameter
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.http.*
import com.fasterxml.jackson.databind.*
import com.google.gson.GsonBuilder
import io.ktor.jackson.*
import io.ktor.features.*
import kotlin.test.*
import io.ktor.server.testing.*
//import org.h2.store.Page.insert
import org.junit.jupiter.api.Test


class ApplicationTest {
    @Test
    fun testRoot() {
        withTestApplication({ module(testing = true) }) {
            handleRequest(HttpMethod.Get, "/").apply {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals("HELLO WORLD!", response.content)
            }
        }
    }

//    成功テスト（get）
    @Test
    fun testGetTodos(){
        withTestApplication(Application::module) {
            handleRequest(HttpMethod.Get, "/todos").apply {
                assertEquals(HttpStatusCode.OK, response.status())
            }
        }
    }

//    成功テスト（post)
    @Test
    fun testCreateSuccessRequest(){
        val gson = GsonBuilder().setPrettyPrinting().create()
        val param = Todo(title = "test", detail = "test" , date = "2021-12-30")
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Post,"/todos"){
                addHeader(HttpHeaders.Accept, ContentType.Text.Plain.toString())
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
                setBody(gson.toJson(param))
            }.run {
                assertEquals(HttpStatusCode.OK,response.status())
            }
        }
    }

//    成功テスト（Put）
    @Test
//     更新されるけど404エラーが返ってくる
    fun testEditSuccessRequest(){
        val gson = GsonBuilder().setPrettyPrinting().create()
        val param = TodoEditParameter(id = 13, title = "変更1", detail = "変更" , date = "2021-12-30")
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Put,"/todos/13"){
                addHeader(HttpHeaders.Accept, ContentType.Text.Plain.toString())
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
                setBody(gson.toJson(param))
            }.run {
                assertEquals(HttpStatusCode.OK,response.status())
            }
        }
    }

//    成功テスト（Delete）
    @Test
//    削除されるけど404エラーが返ってくる
    fun testDeleteSuccessRequest(){
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Delete,"/todos/14"){
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
            }.run {
                assertEquals(HttpStatusCode.OK,response.status())
            }
        }
    }

}
